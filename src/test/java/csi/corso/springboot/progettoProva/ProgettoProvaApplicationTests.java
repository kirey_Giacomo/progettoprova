package csi.corso.springboot.progettoProva;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.*;

import java.net.URI;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@DataJpaTest
//@AutoConfigureMockMvc
class ProgettoProvaApplicationTests {

	//@Autowired
	//private MockMvc mockMvc;
	
	@Test
	void contextLoads() {
	}
	
	@Test
	@WithMockUser(username = "admin",password = "password",roles ="ADMIN")
	void testAllLayer() throws Exception {
		
		/*MvcResult mvr = mockMvc.perform(MockMvcRequestBuilders.get(URI.create("/api/hello"))).andReturn();
		System.out.println();
		System.out.println(mvr.getRequest().getPathInfo());
		System.out.println( mvr.getResponse().getStatus());
		*/
		System.out.println("CONTESTO JPA PER IL TESTING DELLO SLICE DI DATABASE: Logica busness+ Query DB");
	}

	@Test
	@WithMockUser(username = "admin",password = "password",roles ="ADMIN")
	void test2AllLayer() throws Exception {
		
		/*MvcResult mvr = mockMvc.perform(MockMvcRequestBuilders.get(URI.create("/api/hello"))).andReturn();
		System.out.println();
		System.out.println(mvr.getRequest().getPathInfo());
		System.out.println( mvr.getResponse().getStatus());
		*/
		System.out.println("CONTESTO JPA PER IL TESTING DELLO SLICE DI DATABASE: Logica busness+ Query DB");
	}	
	
}
