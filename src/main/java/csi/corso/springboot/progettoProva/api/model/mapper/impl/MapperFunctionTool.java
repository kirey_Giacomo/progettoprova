package csi.corso.springboot.progettoProva.api.model.mapper.impl;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import csi.corso.springboot.progettoProva.api.model.generated.Category;
import csi.corso.springboot.progettoProva.api.model.generated.Pet;
import csi.corso.springboot.progettoProva.api.model.generated.Tag;
import csi.corso.springboot.progettoProva.entities.CategoryEntity;
import csi.corso.springboot.progettoProva.entities.PetEntity;
import csi.corso.springboot.progettoProva.entities.PhotoUrlEntity;
import csi.corso.springboot.progettoProva.entities.TagEntity;

public class MapperFunctionTool {

	private MapperFunctionTool() {
		throw new IllegalStateException("Utility class");
	}

	public static final Function<TagEntity,Tag> toTag = te-> {
		Tag t = new Tag();
		t.setName(te.getName());
		return t;
	};
	
	
	public static final Function<CategoryEntity,Category> toCategory = ce -> {
		Category c = new Category();
		c.setId(ce.getId());
		c.setName(ce.getName());
		return c;
	};
	
	public static final Function<PetEntity, Pet> toPet = pe -> {
		Pet p = new Pet();
		Optional.ofNullable(pe.getPhotoUrls()).ifPresent(e->
			p.setPhotoUrls(e.stream().map(PhotoUrlEntity::getPhotourl).collect(Collectors.toList()))
		);
		Optional.ofNullable(pe.getTags()).ifPresent(e -> 
			p.setTags( e.stream().map(toTag).collect(Collectors.toList()))
		);
		Optional.ofNullable(pe.getCategory()).map(toCategory).ifPresent(p::setCategory);
		p.setStatus(pe.getStatusEnum());
		p.setName(pe.getName());		
		p.setId(pe.getId());
		return p;
	};

	public static final Function<Category,CategoryEntity> toCategoryEntity = c -> {
		CategoryEntity ce = new CategoryEntity();
		ce.setId(c.getId());
		ce.setName(c.getName());
		return ce;	
	}; 
	
	public static final Function<Tag,TagEntity> toTagEntity = t -> {
		TagEntity te = new TagEntity();
		te.setName(t.getName());
		return te;
	};
	
	public static final Function<Pet,PetEntity> toPetEntity = p -> {
		PetEntity pe = new PetEntity();
		Optional.ofNullable(p.getCategory()).map(toCategoryEntity).ifPresent(pe::setCategory);
		Optional.ofNullable(p.getTags()).ifPresent(e -> 
			pe.setTags( e.stream().map(toTagEntity).collect(Collectors.toList()))
		);
		Optional.ofNullable(p.getPhotoUrls()).ifPresent(e->
			pe.setPhotoUrls(e.stream().map(PhotoUrlEntity::new).collect(Collectors.toList()))
		);
		pe.setStatusEnum(p.getStatus());
		pe.setName(p.getName());
		pe.setId(p.getId());
		return pe;
	};

	
	
}
