package csi.corso.springboot.progettoProva.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/hello")
public interface HelloService {

    @GET
    @Consumes({ "application/json"})
    @Produces({ "application/json"})
	public Response sayHello();
	
}
