package csi.corso.springboot.progettoProva.api.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import csi.corso.springboot.progettoProva.api.HelloService;
import csi.corso.springboot.progettoProva.beans.SingletonBean;

@Component
public class HelloServiceImp implements HelloService {

	@Autowired
	private SingletonBean singletonBean;

	
		
	@Override
	public Response sayHello() {
		singletonBean.getPrototypeBean();
		return Response.ok(singletonBean.getPrototypeBean()).build();
	}

}
