package csi.corso.springboot.progettoProva.beans;


import java.io.Serializable;
import java.time.LocalTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;

import lombok.Data;
@Data
public class PrototypeBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonSerialize(using = LocalTimeSerializer.class)
	@JsonDeserialize(using = LocalTimeDeserializer.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss.SSS")
	private LocalTime myTime;


}
