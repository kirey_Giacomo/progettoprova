package csi.corso.springboot.progettoProva.beans;

import java.time.LocalTime;

import javax.inject.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;


 //@Component//1
public class SingletonBean {

	Logger logger = LoggerFactory.getLogger(SingletonBean.class);
	
    @Autowired //4
    private ObjectFactory<PrototypeBean> prototypeBeanObjectFactory;

	//4
    public PrototypeBean getPrototypeBean()  {
        logger.info(String.valueOf(LocalTime.now()));
        PrototypeBean prototypeBean = prototypeBeanObjectFactory.getObject();
        logger.info("{}",prototypeBean);
        return  prototypeBean;
    }
    
	
	/*
	@Autowired //0 //3
    private PrototypeBean prototypeBean;

	
	
	//@Autowired
    public SingletonBean() {
        logger.info("Singleton instance created");
    }

     //0 //3
    /*
    public PrototypeBean getPrototypeBean() {
        logger.info(String.valueOf(LocalTime.now()));
        logger.info("{}",prototypeBean);
        return prototypeBean;
    }
   /*
    
    
    /*
    @Lookup //1 - Cosa succede se tolgo l'annotation Component? 
    public PrototypeBean getPrototypeBean() {
    	return null;
    }
   */ 
       
    
/*    
    @Autowired //2
    private Provider<PrototypeBean> myPrototypeBeanProvider;
        

    //2
    public PrototypeBean getPrototypeBean() {
        return myPrototypeBeanProvider.get();
    }

*/ 	

    /* FUNCTIONAL
    @Autowired
    private Function<String, PrototypeBean> beanFactory;
    
    public PrototypeBean getPrototypeInstance(String name) {
        PrototypeBean bean = beanFactory.apply(name);
        return bean;
    }
    */
    
    
}
