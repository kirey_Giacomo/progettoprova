package csi.corso.springboot.progettoProva.configurations.security.custom_annotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.security.access.prepost.PreAuthorize;

@Retention(RUNTIME)
@Target(METHOD)
@PreAuthorize("hasRole('USER')")
public @interface IsUser {

}
