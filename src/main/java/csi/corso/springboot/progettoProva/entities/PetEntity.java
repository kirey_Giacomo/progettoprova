package csi.corso.springboot.progettoProva.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import csi.corso.springboot.progettoProva.api.model.generated.Pet;


@Entity
@Table(name="pets")
public class PetEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
    @NotNull
    private String name;
	
    @ManyToOne
    @NotNull // -> usa la specifica delle validazioni
    //@Column(nullable=false)// -> usa la specifica JPA
    private CategoryEntity category;
	
    @ElementCollection
    @CollectionTable(name = "pets_photourls", joinColumns = @javax.persistence.JoinColumn(name = "pet_id"))
    private List<PhotoUrlEntity> photoUrls=new ArrayList<>();
	  
    @ElementCollection
    @CollectionTable(name = "pets_tags", joinColumns = @javax.persistence.JoinColumn(name = "pet_id"),uniqueConstraints = {@UniqueConstraint(columnNames = {"pet_id","name"})})
    private List<TagEntity> tags = new ArrayList<>();

    
    @Enumerated(EnumType.STRING)
    @NotNull
    private Pet.StatusEnum statusEnum;
    
	public Pet.StatusEnum getStatusEnum() {
		return statusEnum;
	}

	public void setStatusEnum(Pet.StatusEnum statusEnum) {
		this.statusEnum = statusEnum;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	public CategoryEntity getCategory() {
		return category;
	}

	public void setCategory(CategoryEntity category) {
		this.category = category;
	}

	public List<PhotoUrlEntity> getPhotoUrls() {
		return photoUrls;
	}

	public void setPhotoUrls(List<PhotoUrlEntity> photoUrls) {
		this.photoUrls = photoUrls;
	}

	public List<TagEntity> getTags() {
		return tags;
	}

	public void setTags(List<TagEntity> tags) {
		this.tags = tags;
	}

	
	
}
