package csi.corso.springboot.progettoProva.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="categories")
public class CategoryEntity {
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)  
	 private Long id;
	 @NotNull
	 @Column(unique=true)
	 private String name;
	 
	 @OneToMany(mappedBy = "category")
	 private List<PetEntity> pets = new ArrayList<>();
	 
	public String getName() {
		return name;
	 }
	
	 public void setName(String name) {
		this.name = name;
	 }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
