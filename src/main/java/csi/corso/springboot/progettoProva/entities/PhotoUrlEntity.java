package csi.corso.springboot.progettoProva.entities;

import javax.persistence.Embeddable;

@Embeddable
public class PhotoUrlEntity {

	private String photourl;

	public PhotoUrlEntity() {
		super();
	}

	public PhotoUrlEntity(String photourl) {
		super();
		this.photourl = photourl;
	}

	public String getPhotourl() {
		return photourl;
	}

	public void setPhotourl(String photourl) {
		this.photourl = photourl;
	}
}
