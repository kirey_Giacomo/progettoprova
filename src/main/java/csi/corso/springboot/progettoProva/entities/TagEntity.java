package csi.corso.springboot.progettoProva.entities;

import javax.persistence.Embeddable;

@Embeddable
public class TagEntity {
	  private String name = null;

	public TagEntity(String name) {
		super();
		this.name = name;
	}

	public TagEntity() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
