package csi.corso.springboot.progettoProva.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import csi.corso.springboot.progettoProva.entities.PetEntity;

public interface PetRepository extends JpaRepository<PetEntity, Long> {

	
}
