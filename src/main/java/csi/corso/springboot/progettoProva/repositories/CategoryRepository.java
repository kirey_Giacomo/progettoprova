package csi.corso.springboot.progettoProva.repositories;

import javax.persistence.NamedNativeQuery;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import csi.corso.springboot.progettoProva.entities.CategoryEntity;
@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity,Long> {


}
