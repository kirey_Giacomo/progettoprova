package csi.corso.springboot.progettoProva.services;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import csi.corso.springboot.progettoProva.api.model.generated.Pet;
import csi.corso.springboot.progettoProva.api.model.mapper.EntityMapper;
import csi.corso.springboot.progettoProva.api.model.mapper.impl.MapperFunctionTool;
import csi.corso.springboot.progettoProva.configurations.security.custom_annotations.IsAdmin;
import csi.corso.springboot.progettoProva.entities.PetEntity;
import csi.corso.springboot.progettoProva.repositories.PetRepository;

@Service
public class PetServiceImpl {

	private PetRepository petRepository;
	private Validator validator;
	
	private static final Logger log = LoggerFactory.getLogger(PetServiceImpl.class);
	
	@Autowired
	public PetServiceImpl(PetRepository petRepository,Validator validator ){
		this.petRepository = petRepository;
		this.validator = validator;
	}

	
	@Transactional
	@IsAdmin
	public Pet savePet(Pet p) {
		Set<ConstraintViolation<Pet>> violations = validator.validate(p);
		if(!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (ConstraintViolation<Pet> constraintViolation : violations) {
                sb.append(constraintViolation.getMessage());
            }
            throw new ConstraintViolationException("Error occurred: " + sb.toString(), violations);
		}
		PetEntity pe = petRepository.save(EntityMapper.map(p,MapperFunctionTool.toPetEntity) );
		p.setId(pe.getId());
		return p;
		
		
	}
	
	@Transactional
	public Pet getById(Long id) {
		return EntityMapper.map(petRepository.getReferenceById(id),MapperFunctionTool.toPet);	
	}
	
	@Async
	@IsAdmin
	public void asyncFunction() {
		Authentication authObj = SecurityContextHolder.getContext().getAuthentication();
		log.info("Inside the @Async logic: {} " 
			    , authObj == null ? "Authentication is null": authObj.getPrincipal());
	}
	
	
}
