package csi.corso.springboot.progettoProva.services.handlers;

import javax.persistence.EntityNotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Provider
@Component
public class EntityNotFoundExceptionHandler  implements ExceptionMapper<EntityNotFoundException> {
	private static final Logger log = LoggerFactory.getLogger(EntityNotFoundExceptionHandler.class);
	
	@Override
	public Response toResponse(EntityNotFoundException e) {
		 log.error("Exiting with error code: {} message {}", HttpStatus.SC_NOT_FOUND, e.getMessage());
	     return Response.status(HttpStatus.SC_NOT_FOUND).entity(e.getMessage()).build();
	}

}
