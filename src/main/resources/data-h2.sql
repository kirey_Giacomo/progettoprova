INSERT into categories (name) values ('GATTO');
INSERT into categories (name) values ('SERPENTE');
INSERT into categories (name) values ('CANE');
INSERT into categories (name) values ('CRICETO');

INSERT INTO pets ( name,category_id,status_enum)values ('Nando',(SELECT id FROM categories WHERE name = 'GATTO'),'AVAILABLE'  );
INSERT INTO pets ( name,category_id,status_enum)values ('Ciancalino',(SELECT id FROM categories WHERE name = 'GATTO'),'PENDING'  );
INSERT INTO pets ( name,category_id,status_enum)values ('Rollo',(SELECT id FROM categories WHERE name = 'CANE'),'SOLD'  );
INSERT INTO pets ( name,category_id,status_enum)values ('Fido',(SELECT id FROM categories WHERE name = 'CANE'),'AVAILABLE'  );
INSERT INTO pets ( name,category_id,status_enum)values ('BisciaLiscia',(SELECT id FROM categories WHERE name = 'SERPENTE'),'PENDING'  );
INSERT INTO pets ( name,category_id,status_enum)values ('ElDiablo',(SELECT id FROM categories WHERE name = 'SERPENTE'),'SOLD'  );
INSERT INTO pets ( name,category_id,status_enum)values ('Hamtaro',(SELECT id FROM categories WHERE name = 'CRICETO'),'AVAILABLE');
INSERT INTO pets ( name,category_id,status_enum)values ('Maneater',(SELECT id FROM categories WHERE name = 'CRICETO'),'PENDING');

INSERT INTO pets_tags(name,pet_id)values ('Vivace',1);
INSERT INTO pets_tags(name,pet_id)values ('Giocherellone',1);
INSERT INTO pets_tags(name,pet_id)values ('Giocherellone',3);
INSERT INTO pets_tags(name,pet_id)values ('Giocherellone',4);
INSERT INTO pets_tags(name,pet_id)values ('Tranquillo',7);
INSERT INTO pets_tags(name,pet_id)values ('Mangione',8);

INSERT INTO pets_photourls(photourl,pet_id)values ('foto_nanto_1.png',1);
INSERT INTO pets_photourls(photourl,pet_id)values ('foto_nando_2.png',1);
INSERT INTO pets_photourls(photourl,pet_id)values ('foto_eldiablo_1.png',6);
INSERT INTO pets_photourls(photourl,pet_id)values ('foto_eldiablo_2.png',6);
INSERT INTO pets_photourls(photourl,pet_id)values ('foto_rollo_1.png',3);
INSERT INTO pets_photourls(photourl,pet_id)values ('foto_rollo_2.png',3);
INSERT INTO pets_photourls(photourl,pet_id)values ('foto_rollo_3.png',3);