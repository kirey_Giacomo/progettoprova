drop table if exists categories CASCADE 
drop table if exists pets CASCADE 
drop table if exists pets_photourls CASCADE 
drop table if exists pets_tags CASCADE 
create table categories (id bigint generated by default as identity, name varchar(255) not null, primary key (id))
create table pets (id bigint generated by default as identity, name varchar(255) not null, status_enum varchar(255) not null, category_id bigint not null, primary key (id))
create table pets_photourls (pet_id bigint not null, photourl varchar(255))
create table pets_tags (pet_id bigint not null, name varchar(255))
alter table categories add constraint UK_CATERGORY_NAME unique (name)
alter table pets_tags add constraint UK_PET_TAG_BY_PET unique (pet_id, name)
alter table pets add constraint FK_PETS_CATEGORY foreign key (category_id) references categories
alter table pets_photourls add constraint FK_PET_PHOTOURLS foreign key (pet_id) references pets
alter table pets_tags add constraint FK2_PET_PET_TAGS foreign key (pet_id) references pets